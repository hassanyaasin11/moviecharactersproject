package com.example.moviecharactersproject.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.ToString;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@ToString
@Entity
public class Movies {

    @Id
    @GeneratedValue(
            strategy = GenerationType.IDENTITY
    )
    private int id;

    private String title;
    private String genre;
    private String releaseYear;
    private String director;
    private String picture;
    private String trailer;

    @ManyToMany(mappedBy = "movies")
    private Set<MovieCharacter> movieCharacter = new HashSet<>();

    @ManyToOne
    @JoinColumn(name = "franchises_id")
    private Franchises franchises;



    public int getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public String getReleaseYear() {
        return releaseYear;
    }

    public void setReleaseYear(String releaseYear) {
        this.releaseYear = releaseYear;
    }

    public String getDirector() {
        return director;
    }

    public void setDirector(String director) {
        this.director = director;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public String getTrailer() {
        return trailer;
    }

    public void setTrailer(String trailer) {
        this.trailer = trailer;
    }

    public Set<MovieCharacter> getCharacter() {
        return movieCharacter;
    }

    public void setCharacter(Set<MovieCharacter> movieCharacter) {
        this.movieCharacter = movieCharacter;
    }

}
