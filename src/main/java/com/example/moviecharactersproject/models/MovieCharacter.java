package com.example.moviecharactersproject.models;


import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
public class MovieCharacter {

    @Id
    @GeneratedValue(
            strategy = GenerationType.IDENTITY
    )
    private int id;

    private String fullName;
    private String alias;
    private String gender;
    private String profilePic;


    @ManyToMany
    @JoinTable(name = "movie_character_movies",
    joinColumns = {@JoinColumn(name = "movie_character_id")},
    inverseJoinColumns = {@JoinColumn(name = "movies_id")})
    private Set<Movies> movies;



    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getProfilePic() {
        return profilePic;
    }

    public void setProfilePic(String profilePic) {
        this.profilePic = profilePic;
    }

    public Set<Movies> getMovies() {
        return movies;
    }

    public void setMovies(Set<Movies> movies) {
        this.movies = movies;
    }
}
