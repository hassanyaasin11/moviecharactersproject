package com.example.moviecharactersproject;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MovieCharactersProjectApplication {

    public static void main(String[] args) {
        SpringApplication.run(MovieCharactersProjectApplication.class, args);
    }

}
