package com.example.moviecharactersproject.controllers;


import com.example.moviecharactersproject.Dto.MovieDto;
import com.example.moviecharactersproject.mappers.MovieMapper;
import com.example.moviecharactersproject.models.MovieCharacter;
import com.example.moviecharactersproject.models.Movies;
import com.example.moviecharactersproject.services.MovieService;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.Set;

@AllArgsConstructor
@RestController
@RequestMapping(path = "api/v1/movies")
public class MovieController {


    MovieService movieService;
    MovieMapper movieMapper;


    @GetMapping()
    public ResponseEntity findAllMovies(){
        Collection<MovieDto> movies = movieMapper.moviesToMovieDto(
                movieService.findAll()
        );
        return ResponseEntity.ok(movies);
    }

    @GetMapping("/{id}")
    public ResponseEntity findById(@PathVariable int id) {
        MovieDto movie = movieMapper.moviesToMovieDto(
                movieService.findById(id)
        );
        return ResponseEntity.ok(movie);
    }

    @PostMapping()
    public ResponseEntity add(@RequestBody Movies movies){
        Movies newMovie = movieService.add(movies);
        URI uri = URI.create("movies/" + newMovie.getId());
        return ResponseEntity.created(uri).build();
    }

    @PutMapping("/{id}")
    public ResponseEntity update(@RequestBody MovieDto movieDto, @PathVariable int id){
        if(id != movieDto.getMovieId()){
            return ResponseEntity.badRequest().build();
        }
        movieService.update(
                movieMapper.movieDtoToMovie(movieDto)
        );
        return ResponseEntity.noContent().build();
    }

    @DeleteMapping("/{id}")
    public ResponseEntity delete(@PathVariable int id){
        movieService.delete(id);
        return ResponseEntity.noContent().build();
    }

}
