package com.example.moviecharactersproject.controllers;

import com.example.moviecharactersproject.Dto.MovieCharacterDto;
import com.example.moviecharactersproject.mappers.MovieCharacterMapper;
import com.example.moviecharactersproject.models.MovieCharacter;
import com.example.moviecharactersproject.services.MovieCharacterService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.Collection;

@AllArgsConstructor
@RestController
@RequestMapping(path = "api/v1/characters")
public class MovieCharacterController {

    MovieCharacterService movieCharacterService;
    MovieCharacterMapper movieCharacterMapper;


    @GetMapping
    public ResponseEntity findAllCharacters() {

        Collection<MovieCharacterDto> characters = movieCharacterMapper.movieCharacterToMovieCharacterDto(
                movieCharacterService.findAllCharacters()
        );
        return ResponseEntity.ok(characters);
    }

    @GetMapping("{id}")
    public ResponseEntity findCharacterById (@PathVariable int id) {
        MovieCharacterDto character = movieCharacterMapper.movieCharacterToMovieCharacterDto(

                movieCharacterService.findCharacterById(id)
        );
        return ResponseEntity.ok(character);
    }

    @PostMapping
    public  ResponseEntity add(@RequestBody MovieCharacter movieCharacter) {
        MovieCharacter newCharacter = movieCharacterService.add(movieCharacter);
        URI location = URI.create("characters/" + newCharacter.getId());
        return ResponseEntity.created(location).build();
    }

  /*  @GetMapping
    public ResponseEntity findCharacterByName (@RequestParam MovieCharacter name) {
        return ResponseEntity.ok(movieCharacterService.findCharacterByName(MovieCharacter.name));
    }*/
}
