package com.example.moviecharactersproject.controllers;

import com.example.moviecharactersproject.Dto.FranchiseDto;
import com.example.moviecharactersproject.mappers.FranchiseMapper;
import com.example.moviecharactersproject.models.Franchises;
import com.example.moviecharactersproject.services.FranchisesService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collection;


@AllArgsConstructor
@RestController
@RequestMapping(path = "api/v1/franchises")
public class FranchisesController {

        FranchiseMapper franchisesMapper;
        FranchisesService franchisesService;



    @GetMapping
    public ResponseEntity<Collection<Franchises>> getAll() {

        return ResponseEntity.ok(franchisesService.getAllFranchises());

    }

    @GetMapping("{id}")
    public ResponseEntity findOneFanchises (@PathVariable int id) {
        return ResponseEntity.ok(franchisesService.getFranchiseIdsBy(id));
    }

    @GetMapping("{id}")
    public ResponseEntity findById(@PathVariable int id) {
        FranchiseDto fran = franchisesMapper.FranchisesToFranchiseDto(
                franchisesService.getFranchiseIdsBy(id)
        );
        return ResponseEntity.ok(fran);
    }



}



