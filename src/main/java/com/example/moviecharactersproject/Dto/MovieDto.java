package com.example.moviecharactersproject.Dto;


import lombok.Data;
import lombok.Value;

import java.util.List;
import java.util.Set;

@Data
public class MovieDto {
    private int movieId;
    private String title;
    private String genre;
    private String release;
    private int franchise;
    Set<Integer> characters;

}
