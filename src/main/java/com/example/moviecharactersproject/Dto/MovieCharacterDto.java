package com.example.moviecharactersproject.Dto;


import lombok.Data;

import java.util.Set;

@Data
public class MovieCharacterDto {
    private int characterId;
    private String alias;
    private String fullName;
    Set<Integer> Movies;
}
