package com.example.moviecharactersproject.Dto;


import lombok.Data;

import java.util.Set;

@Data
public class FranchiseDto {
    private String franchiseId;
    private String name;
    Set<Integer> Movies;
}
