package com.example.moviecharactersproject.services;

import com.example.moviecharactersproject.exceptions.MovieNotFoundException;
import com.example.moviecharactersproject.models.Movies;
import com.example.moviecharactersproject.repo.MovieRepo;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.List;
import java.util.Optional;


@AllArgsConstructor
@Service
public class MovieService  {


    MovieRepo movieRepo;


    public Movies findById(int id){
        return movieRepo.findById(id)
                .orElseThrow(() -> new MovieNotFoundException(id));
    }

    public Collection<Movies> findAll(){
        return movieRepo.findAll();
    }


    public Movies add(Movies movies){
        return movieRepo.save(movies);
    }

    public Movies update(Movies movies){
        return movieRepo.save(movies);
    }

    public void delete(int id){
        movieRepo.deleteById(id);
    }
}
