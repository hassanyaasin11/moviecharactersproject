package com.example.moviecharactersproject.services;

import com.example.moviecharactersproject.models.Franchises;
import com.example.moviecharactersproject.models.MovieCharacter;
import com.example.moviecharactersproject.repo.FranchisesRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.Collections;
import java.util.Optional;
import java.util.Set;


@Service
public class FranchisesService {
    private final FranchisesRepo franchisesRepo;

    public FranchisesService(FranchisesRepo franchisesRepo) {

        this.franchisesRepo = franchisesRepo;
    }
/*
    public Optional<Franchises> findFranchisesByName(String title) {

        return franchisesRepo.findBy(title);
    }*/

    public Collection<Franchises> getAllFranchises() {

        return franchisesRepo.findAll();
    }

    public Franchises getFranchiseIdsBy(int id) {

        return franchisesRepo.findById(id).orElseThrow();
    }


}
