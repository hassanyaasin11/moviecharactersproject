package com.example.moviecharactersproject.services;

import com.example.moviecharactersproject.models.MovieCharacter;
import com.example.moviecharactersproject.repo.MovieCharacterRepo;
import org.springframework.stereotype.Service;

import java.util.Collection;


@Service
public class MovieCharacterService {


  private final MovieCharacterRepo movieCharacterRepo;

  public MovieCharacterService(MovieCharacterRepo movieCharacterRepo) {
    this.movieCharacterRepo = movieCharacterRepo;
  }


/*  public Set<MovieCharacter> findMovieCharacterByMovies(String title) {
      return movieCharacterRepo.findMovieCharacterByMovies(title);
    }*/

  public Collection<MovieCharacter> findAllCharacters() {

    return movieCharacterRepo.findAll();
  }

  public MovieCharacter findCharacterById(Integer id) {
    /*
    return (Set<MovieCharacter>) movieCharacterRepo.getCharacterById(id);
  }*/
    return movieCharacterRepo.findById(id).orElseThrow();
  }

  public MovieCharacter add(MovieCharacter movieCharacter) {
    return  movieCharacterRepo.save(movieCharacter);
  }

 public String findCharacterByName(String name) {

    return movieCharacterRepo.findCharacterByName(name);
  }

}
