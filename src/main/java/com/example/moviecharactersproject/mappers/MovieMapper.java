package com.example.moviecharactersproject.mappers;


import com.example.moviecharactersproject.Dto.MovieDto;
import com.example.moviecharactersproject.models.Franchises;
import com.example.moviecharactersproject.models.MovieCharacter;
import com.example.moviecharactersproject.models.Movies;
import com.example.moviecharactersproject.services.FranchisesService;
import com.example.moviecharactersproject.services.MovieCharacterService;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;

import javax.security.auth.Subject;
import java.util.Collection;
import java.util.Set;
import java.util.stream.Collectors;

@Mapper(componentModel = "spring")
public abstract class MovieMapper {

    @Autowired
    protected MovieCharacterService movieCharacterService;

    @Autowired
    protected FranchisesService franchisesService;

    @Mapping(target = "moviecharacter", source = "moviecharacter.id", qualifiedByName = "movieCharactersToIds")
    @Mapping(target = "franchises", source = "franchises.id")
    public abstract MovieDto moviesToMovieDto(Movies movies);


    public abstract Collection<MovieDto> moviesToMovieDto(Collection<Movies> movies);

    @Mapping(target = "franchises", source = "franchises", qualifiedByName = "franchisesIdTofranchises")
    @Mapping(target = "moviecharacter", source = "moviecharacter", qualifiedByName = "moviecharacterIdToMoviecharacter")
    public abstract Movies movieDtoToMovie(MovieDto dto);


    @Named("franchisesIdTofranchises")
    Franchises mapIdToFranchises(int id) {
        return franchisesService.findById(id);
    }

    @Named("moviecharacterIdToMoviecharacter")
    Set<MovieCharacter> mapIdsToMovieCharacter(Set<Integer> id) {
        return id.stream()
                .map( i -> movieCharacterService.findById(i))
                .collect(Collectors.toSet());
    }

    @Named("moviecharacterToIds")
    Set<Integer> mapmoviecharacterToIds(Set<MovieCharacter> source) {
        if(source == null)
            return null;
        return source.stream()
                .map(s -> s.getId()).collect(Collectors.toSet());
    }





}
