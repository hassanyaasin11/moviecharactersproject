package com.example.moviecharactersproject.mappers;

import com.example.moviecharactersproject.Dto.MovieCharacterDto;
import com.example.moviecharactersproject.models.MovieCharacter;
import com.example.moviecharactersproject.models.Movies;
import com.example.moviecharactersproject.services.MovieService;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import org.springframework.beans.factory.annotation.Autowired;

import javax.security.auth.Subject;
import java.util.Collection;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;


@Mapper(componentModel = "spring")
public abstract class MovieCharacterMapper {


        @Autowired
        protected MovieService movieService;

        @Mapping(target = "movies", source = "movies.id", qualifiedByName = "moviesToIds")
        public abstract MovieCharacterDto movieCharacterToMovieCharacterDto(MovieCharacter movieCharacter);


        public abstract Collection<MovieCharacterDto> movieCharacterToMovieCharacterDto(Collection<MovieCharacter> movieCharacter);


        @Mapping(target = "movies", source = "movies", qualifiedByName = "moviesIdToMovies")
        public abstract MovieCharacter movieCharacterDtoToMovieCharacter(MovieCharacterDto dto);



        @Named("moviesIdToMovies")
        Set<Movies> mapIdsToMovies(Set<Integer> id) {
            return id.stream()
                    .map( i -> movieService.findById(i))
                    .collect(Collectors.toSet());
        }

    @Named("moviesToIds")
    Set<Integer> mapMoviesToIds(Set<Movies> source) {
        if(source == null)
            return null;
        return source.stream()
                .map(s -> s.getId()).collect(Collectors.toSet());
    }




}
