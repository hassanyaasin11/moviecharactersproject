package com.example.moviecharactersproject.repo;

import com.example.moviecharactersproject.models.MovieCharacter;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Set;


@Repository
public interface MovieCharacterRepo extends JpaRepository<MovieCharacter, Integer> {
/*  *//*  @Query("SELECT c.fullName, c.alias, c.gender, c.profilePic, m.title, m.genre, m.picture, m.releaseYear \n" +
            "FROM MovieCharacter c\n" +
            "INNER JOIN LinkCharacterMovies AS l ON l.characterId=c.id\n" +
            "INNER JOIN Movies AS m ON m.id=l.movieId")
    Set<MovieCharacter> findMovieCharacterByMovies(String title);*//*

    @Query("SELECT mc FROM MovieCharacter mc")
    Set<MovieCharacter> getAllCharacters();


    @Query("SELECT mc FROM MovieCharacter mc WHERE mc.id=?1")
    Set<MovieCharacter> findCharacterById(int id);
*/
    @Query("SELECT fullName FROM MovieCharacter ")
    String findCharacterByName(String name);
}

