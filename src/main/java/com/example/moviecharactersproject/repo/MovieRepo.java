
package com.example.moviecharactersproject.repo;

import com.example.moviecharactersproject.models.Movies;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface MovieRepo extends JpaRepository<Movies, Integer> {


//    @Query("SELECT m.title, m.director FROM Movies m  WHERE m.id = ?1")
//    Optional<Movies> findMoviesById(int id);
}

